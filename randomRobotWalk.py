import pygame 
import random
num_tiles = 10
margin = 5


board = [[0]*num_tiles for i in range(num_tiles)]

counter = 0
while counter <= 20:
    x = random.choice(range(10))
    y= random.choice(range(10))
    board[x][y] = 'obs'
    counter += 1
    
row = random.choice(range(10))
col = random.choice(range(10))
board[row][col]='robot'
Hight = 50
Width = 50

color_empty = (242, 235, 191)
color_obs = (92, 75, 81)
color_robot = (240, 96, 96)
white = (255, 255, 255)
quit = False
pygame.init()
size = (((num_tiles/2.0)*100)+((num_tiles+2)*margin)-margin)
screen= pygame.display.set_mode((int(size),int(size)))
pygame.display.set_caption("Random walk ")

def rand_move():
    coin=random.randint(0,1)
    if coin == 0:
        direction= -1
    else:
        direction=1
    return direction


def rand_dir():
    coin=random.randint(0,1)
    if coin == 0:
        new_dir='h'
    else:
        new_dir='v'
    return new_dir


fps= pygame.time.Clock()

while quit == False:
    for event in pygame.event.get():
        
        if event.type == pygame.QUIT:
            quit = True
        if event.type == pygame.MOUSEBUTTONDOWN:
           
                
            pastcol = col
            pastrow = row
            is_empty = False
            in_border = False
            while is_empty != True or in_border != True:
                new_dir = rand_dir()
                new_move = rand_move() 
                if new_dir == "v":
                    col += new_move
                else:
                    row +=new_move                                        
                if col <= num_tiles-1 and col >= 0 and row <= (num_tiles-1) and row >= 0:
                    in_border = True 
                    if board[row][col] == 'obs':
                        is_empty = False
                        col = pastcol
                        row = pastrow                           
                    else:
                        is_empty = True
                        board[pastrow][pastcol]=0
                        board[row][col]= 'robot'
                else: 
                    is_empty = False
                    col = pastcol
                    row = pastcol
                
    
    for rows in range(len(board)):
        for cols in range(len(board)):
            color = (92, 75, 81 )
            if board[rows][cols] == 'obs':
                color = (154, 57, 189)
            elif board[rows][cols] == 'robot':
                color = (189, 57, 96)
            pygame.draw.rect(screen, color,[(Hight+margin)*rows+margin,(Hight+margin)*cols+margin,Hight,Width])      
    pygame.display.update()
    fps.tick(60)
pygame.quit()