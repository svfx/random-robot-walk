# README #

A Python based project implementing a random robot walk wwhilst avoiding obstacles. The project was created during our sophomore (undergraduate) year at Nile University, Egypt.

### Team ###

* Marwan Ahmed [Mechanical Engineering] :: m.ahmed@nu.edu.eg
* Rawan Essam [Computer Engineering] :: r.essam@nu.edu.eg
* Shereen Farag [Computer Engineering] :: sh.alaa@nu.edu.eg

### Program Details ###
* Python 2.7 was used to implement this program
* Pygame is a required and imported library/package and can be installed via terminal by: pip install pygame
